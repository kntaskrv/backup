# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
  2.6.5

* System dependencies
    You need install docker and docker-compose on your system

* Steps for run app
  0)use: docker-compose up --build for first building application

  1)use: dokcer-compose docker-compose -f docker-compose_development.yml up for starting app in
    development env

    use: dokcer-compose docker-compose up for starting app in production env

* Database creation
    Db created after building application

* How to run the test suite
    for start tests use: docker-compose exec app bundle exec rspec

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions
    use: ansible-playbook deploy/ansible/playbook_deploy_to_remote_server.yml for running deployment

* ..
