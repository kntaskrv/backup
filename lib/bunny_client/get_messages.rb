module BunnyClient
  class GetMessages < BaseLibApplication
    param :queue_name

    def call
      self.result = []
      get_messages
      result
    end

    private

    def get_messages
      bunny_client.start
      queue.subscribe do |_delivery_info, _properties, body|
        result << body
      end
    ensure
      bunny_client.close
    end

    def queue
      @queue || bunny_client.create_channel.queue(queue_name, durable: true)
    end

    def bunny_client
      @bunny_client ||= Bunny.new(hostname: ENV['RABBIT_HOSTNAME'])
    end
  end
end
