module AverageDirection
  class Calculate < BaseLibApplication
    param :directions

    def call
      count = directions.count
      value = 0
      diff = 180
      directions.each do |direction|
        value += Rails.configuration.application.dig(:wind_directions)[direction]
      end
      value /= count.to_f
      direction = ''
      Rails.configuration.application.dig(:wind_directions).each do |item|
        next if (value - item.last).abs > diff

        diff = (value - item.last).abs
        direction = item.first.to_s.capitalize
      end
      direction
    end
  end
end
