module StickMethod
  class EqualDistribution < BaseLibApplication
    param :distribution
    param :sticks

    def call
      distribution.each_with_object({}) { |(temperature, _), result| result[temperature] = count_stick }
    end

    private

    def count_stick
      sticks / distribution.size
    end
  end
end
