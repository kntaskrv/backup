module StickMethod
  class MergeDistribution < BaseLibApplication
    param :distributions

    def call
      distributions.each_with_object({}) do |(hour, distribution_arr), result|
        result[hour] = {} if distribution_arr.present?
        distribution_arr.each do |distribution|
          distribution.each do |temperature, sticks|
            result[hour][temperature] = result[hour][temperature] || 0
            result[hour][temperature] += sticks
          end
        end
      end
    end
  end
end
