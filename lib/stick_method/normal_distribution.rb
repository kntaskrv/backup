require 'statistics'

module StickMethod
  class NormalDistribution < BaseLibApplication
    param :distribution
    param :sticks

    def call
      temperature_interval = distribution.keys
      avg_temp = (temperature_interval.first + temperature_interval.last) / 2.0
      normal_dist = Distribution::Normal.new(avg_temp, 1)
      (temperature_interval.first..temperature_interval.last).each_with_object({}) do |temp, result|
        result[temp] = (normal_dist.density_function(temp) * sticks).round
      end
    end
  end
end
