module StickMethod
  class MathExpectation < BaseLibApplication
    param :distributions
    def call
      distributions.each_with_object({}) do |(hour, distribution), result|
        @distribution = distribution
        result[hour] = calc_math_expectation
      end
    end

    private

    attr_reader :distribution

    def calc_math_expectation
      math_expectation = 0
      probabilities.each do |temperature, probability|
        math_expectation += temperature * probability
      end
      math_expectation.round
    end

    def probabilities
      sum = distribution.values.sum
      distribution.each_with_object({}) do |(temperature, sticks), result|
        result[temperature] = sticks / sum.to_f
      end
    end
  end
end
