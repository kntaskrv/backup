module Timelib
  class HoursRound < BaseLibApplication
    option :datetime, proc(&:to_time)

    def call
      # Rounds time to hours.
      # For example: 2020-01-09 09:42:42 +0000 will be rounded to 2020-01-09 10:00:00 +0000 and
      #              2020-01-09 09:22:42 +0000 will be rounded to 2020-01-09 09:00:00 +0000
      datetime_minutes > 30 ? beginning_of_next_hour : datetime.beginning_of_hour
    end

    private

    def beginning_of_next_hour
      (datetime + 1.hour).beginning_of_hour
    end

    def datetime_minutes
      @datetime_minutes ||= datetime.strftime('%M').to_i
    end
  end
end
