module Timelib
  class Detector < BaseLibApplication
    attr_reader :datetime, :timezone_offset

    def initialize(timezone_offset = 0, datetime = DateTime.now)
      @datetime = datetime
      @timezone_offset = timezone_offset.to_i
    end

    def call
      datetime.new_offset(Rational(timezone_offset, 24))
    end
  end
end
