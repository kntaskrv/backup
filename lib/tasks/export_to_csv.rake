# rake task for export prediction data to csv file
# use: rake export_to_csv FILE=filename

task export_to_csv: :environment do
  Prediction.all.each do |p|
    next unless [0, 6, 12, 18].include?(p.parsed_at.hour)

    fw = FactualWeather.all.reverse.find { |f| (p.parsed_at - f.parsed_at).abs < 100 }
    ExportToCsv.call(file_name: ENV['FILE'], data: [p.source.name, p.parsed_at, p.prediction_time, p.predicted_temperature, fw&.temperature])
    Source.five_days.pluck(:id).each do |id|
      op = Prediction.where(source_id: id, prediction_time: p.prediction_time).reverse.find { |pred| (pred.parsed_at - p.parsed_at).abs < 100 }
      ExportToCsv.call(file_name: ENV['FILE'], data: [op.source.name, op.parsed_at, op.prediction_time, op.predicted_temperature, fw&.temperature])
    end
  end
end
