# rake task for recalc source rates of all locations
# use: rake recalc_rates

task recalc_rates: :environment do
  Temperature.all.each do |t|
    fw = FactualWeather.where(location_id: t.location_id).all.find { |f| (f.parsed_at - t.parsed_at).abs < 1000 }
    Rates::MeasurementErrors::Create.call(weather_measure_record: t, factual_weather: fw)
  end
end
