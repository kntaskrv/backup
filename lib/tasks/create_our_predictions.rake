# rake task for create our predictions since defined date and for defined city
# use: rake create_our_predictions DATESTART=date CITY=cityname

task create_our_predictions: :environment do
  datetime = ENV['DATESTART'].to_time
  city = Cities.const_get(ENV['CITY'].capitalize)
  while datetime < Time.now
    Predictions::Create.call(city: city, datetime: datetime)
    datetime += 1.hour
  end
end
