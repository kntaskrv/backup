module Cities
  class Omsk
    def self.name
      'Omsk'
    end

    def self.lat
      54.9641
    end

    def self.long
      73.4179
    end

    def self.timezone_offset
      '+6'
    end
  end

  class Novosibirsk
    def self.name
      'Novosibirsk'
    end

    def self.timezone_offset
      '+7'
    end
  end

  class Krasnodar
    def self.name
      'Krasnodar'
    end

    def self.timezone_offset
      '+3'
    end
  end

  class Moscow
    def self.name
      'Moscow'
    end

    def self.timezone_offset
      '+3'
    end
  end

  class SaintPetersburg
    def self.name
      'Saint-petersburg'
    end

    def self.timezone_offset
      '+3'
    end
  end
end
