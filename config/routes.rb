require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  devise_for :admins, skip: :registration

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  apipie

  # resources :rates, only: %i[index]

  get '/:name', to: 'rates#index'

  namespace :api do
    namespace :v1 do
      resources :rates, only: %i[] do
        get '/current_weather/:name', on: :collection, to: 'rates#current_weather'
        get '/weathers/:name', on: :collection, to: 'rates#weathers'
        get '/our_weathers/:name', on: :collection, to: 'rates#our_weathers'
        get '/global_rates/:name', on: :collection, to: 'rates#global_rates'
      end

      get '/cities',to: 'cities#take_all'
    end
  end

  authenticate :admin do
    mount Sidekiq::Web => '/admins/sidekiq'
    mount PgHero::Engine => '/admins/pghero'
  end

  root 'rates#index'
  
  namespace :admins do
    resources :logs, only: :index do
      get :open, on: :collection
    end
  end
end
