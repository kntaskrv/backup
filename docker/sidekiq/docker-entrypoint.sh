#!/bin/bash
set -e
if [ $RAILS_ENV = "production" ] || [ $RAILS_ENV = "staging" ]; then
  bundle install --without development test
else
  bundle install
fi
exec "$@"