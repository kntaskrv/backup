daemon off;
worker_processes 1;
load_module modules/ngx_http_geoip_module.so;
load_module modules/ngx_stream_geoip_module.so;
load_module modules/ngx_http_geoip2_module.so;
pid /var/run/nginx.pid;

events {
  worker_connections 1024;
}

http {
  include mime.types;
  default_type application/octet-stream;

  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;

  sendfile on;

  tcp_nopush on;
  tcp_nodelay off;

  gzip on;
  gzip_http_version 1.0;
  gzip_proxied any;
  gzip_min_length 500;
  gzip_disable "MSIE [1-6]\.";
  gzip_types text/plain text/xml text/css
             text/comma-separated-values
             text/javascript application/x-javascript
             application/atom+xml;

  upstream rails_app {
    server app:3000;
  }

  geoip2 /etc/nginx/geoip/GeoLite2-City.mmdb {
    $geoip2_data_city_name source=$http_x_real_ip city names en;
  }

  server {
    listen 82;

    root /var/www/data;
    index index.html;

    location /ip {
      default_type text/plain;
      return 200 "$http_x_real_ip $geoip2_data_city_name $request_uri";
    }
    
    location / {
      if ($request_uri = /) {
        rewrite ^ http://$http_x_host/$geoip2_data_city_name;
      }
      try_files $uri $uri/ @backend;
    }

    location ^~/(css|img|js)/ {
      add_header Access-Control-Allow-Origin *;
      gzip_static on;
      expires max;
      add_header Cache-Control public;
    }

    location /assets/ {
      alias /rails_app/public/assets/;
      add_header Access-Control-Allow-Origin *;
      gzip_static on;
      expires max;
      add_header Cache-Control public;
    }
    
    location ~^/(admins|api|rates)/ {
      try_files $uri $uri/ @backend;
    }

    location /apipie {
      try_files $uri $uri/ @backend;
    }

    location @backend {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

      proxy_set_header Host $http_host;

      proxy_redirect off;

      proxy_pass http://rails_app;
    }
  }
}