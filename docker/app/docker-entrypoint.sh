#!/bin/bash
set -e
echo $RAILS_ENV
if [ $RAILS_ENV = "production" ] || [ $RAILS_ENV = "staging" ]; then
  bundle install --without development test
  bundle exec rake db:create
  bundle exec rake db:migrate
else
  bundle install
  bundle exec rake db:create || true
  bundle exec rake db:migrate
fi
bundle exec rake assets:precompile
exec "$@"