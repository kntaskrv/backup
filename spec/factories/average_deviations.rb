FactoryBot.define do
  factory :average_deviation do
    source
    location
    days { Faker::Number.number(digits: 3) }
    average { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
    count { Faker::Number.number(digits: 3) }
  end
end
