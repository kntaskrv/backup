FactoryBot.define do
  factory :global_rate do
    source
    location
    days { Faker::Number.number(digits: 3) }
    rate { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
  end
end
