FactoryBot.define do
  factory :temperature do
    location
    source
    parsed_at { Faker::Time.backward(days: 5) }
    temperature { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
    wind_speed { Faker::Number.number(digits: 3) }
    wind_direction { Faker::String.random(length: 1..3) }
  end
end
