FactoryBot.define do
  factory :source do
    name { Faker::Book.genre }
    url { Faker::Book.genre }
  end
end
