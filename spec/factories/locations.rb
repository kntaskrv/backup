FactoryBot.define do
  factory :location do
    name { Faker::Address.city }
    location_id { Faker::Number.number(digits: 10) }
    latitude { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
    longitude { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
  end
end
