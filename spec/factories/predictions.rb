FactoryBot.define do
  factory :prediction do
    location
    source
    predicted_temperature { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
    prediction_time { Faker::Time.backward(days: 5) }
    parsed_at { Faker::Time.backward(days: 5) }
    days { Faker::Number.number(digits: 2) }
    difference { Faker::Number.decimal(l_digits: 3, r_digits: 3) }
    wind_speed { Faker::Number.number(digits: 3) }
    wind_direction { Faker::String.random(length: 1..3) }
  end
end
