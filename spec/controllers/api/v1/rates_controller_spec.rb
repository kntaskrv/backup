require 'rails_helper'

describe Api::V1::RatesController, type: :request do
  describe 'GET#current_weather' do
    let(:send_request) { get '/api/v1/rates/current_weather/omsk' }

    before do
      host! "localhost:3000"
      expect(Weathers::DetectCurrentWeather).to receive(:call)
      send_request
    end

    it do
      expect(response.status).to eq(200)
    end
  end

  describe 'GET#weathers' do
    let(:send_request) { get '/api/v1/rates/weathers/omsk' }
    let(:location) { create(:location) }

    before do
      host! "localhost:3000"
      expect(Weathers::Detect).to receive(:call).with(location: location)
      send_request
    end

    it do
      expect(response.status).to eq(200)
    end
  end

  describe 'GET#our_weathers' do
    let(:send_request) { get '/api/v1/rates/our_weathers/omsk' }
    let(:location) { create(:location) }

    before do
      host! "localhost:3000"
      expect(Weathers::GetOurWeather).to receive(:call).with(location: location)
      send_request
    end

    it do
      expect(response.status).to eq(200)
    end
  end

  describe 'GET#global_rates' do
    let(:send_request) { get '/api/v1/rates/global_rates/omsk' }
    let(:location) { create(:location) }

    before do
      host! "localhost:3000"
      expect(Rates::Load).to receive(:call)
      send_request
    end

    it do
      expect(response.status).to eq(200)
    end
  end
end
