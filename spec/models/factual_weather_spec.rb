require 'rails_helper'

describe FactualWeather do
  subject { create(:factual_weather) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:parsed_at) }
    it { is_expected.to validate_presence_of(:temperature) }
  end

  describe 'association' do
    it { is_expected.to belong_to(:location) }
  end
end
