require 'rails_helper'

describe Source do
  subject { create(:source) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end

  describe 'association' do
    it { is_expected.to have_many(:locations) }
    it { is_expected.to have_many(:temperatures) }
    it { is_expected.to have_many(:predictions) }
  end
end
