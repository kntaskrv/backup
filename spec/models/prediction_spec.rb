require 'rails_helper'

describe Prediction do
  subject { create(:prediction) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:days) }
    it { is_expected.to validate_presence_of(:parsed_at) }
    it { is_expected.to validate_presence_of(:prediction_time) }
    it { is_expected.to validate_presence_of(:predicted_temperature) }
  end

  describe 'association' do
    it { is_expected.to belong_to(:location) }
  end
end
