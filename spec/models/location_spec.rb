require 'rails_helper'

describe Location do
  subject { create(:location) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe 'association' do
    it { is_expected.to have_many(:temperatures) }
    it { is_expected.to have_many(:predictions) }
  end
end
