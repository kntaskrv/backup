require 'rails_helper'

describe AverageDeviation do
  subject { create(:average_deviation) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:days) }
    it { is_expected.to validate_presence_of(:average) }
    it { is_expected.to validate_presence_of(:count) }
  end

  describe 'association' do
    it { is_expected.to belong_to(:source) }
  end
end
