require 'rails_helper'

describe Temperature do
  subject { create(:temperature) }

  describe 'validations' do
    it { is_expected.to validate_presence_of(:parsed_at) }
    it { is_expected.to validate_presence_of(:temperature) }
  end

  describe 'association' do
    it { is_expected.to belong_to(:location) }
  end
end
