require 'rails_helper'

describe ProcessData do
  subject { described_class.call(data) }

  let(:data) do
    { "source" => "rp5",
      "location" => "Omsk",
      "current_time" => "2019-12-07 08:19:45 +0600",
      "current_temp" => -1.0,
      "wind_speed" => 2,
      "wind_direction" => "WSW",
      "prediction" =>
                  [{ "time" => "2019-12-08 0:00:00", "temperature" => -1.0, "days" => 1 },
                   { "time" => "2019-12-08 3:00:00", "temperature" => -1.0, "days" => 1 },
                   { "time" => "2019-12-08 6:00:00", "temperature" => -2.0, "days" => 1 },
                   { "time" => "2019-12-08 9:00:00", "temperature" => -2.0, "days" => 1 },
                   { "time" => "2019-12-08 12:00:00", "temperature" => 0.0, "days" => 1 },
                   { "time" => "2019-12-08 15:00:00", "temperature" => 1.0, "days" => 1 },
                   { "time" => "2019-12-08 18:00:00", "temperature" => -1.0, "days" => 1 },
                   { "time" => "2019-12-08 21:00:00", "temperature" => -2.0, "days" => 1 }] }
  end

  context 'when valid data' do
    before do
      create(:factual_weather)
    end

    it 'retutns errors' do
      expect(subject[:errors]).to eq []
    end
  end

  context 'when invalid data' do
    context 'when source invalid' do
      let(:data) do
        { "source" => "",
          "location" => "Omsk",
          "current_time" => "2019-12-07 08:19:45 +0600",
          "current_temp" => -1.0,
          "days" => 1,
          "prediction" =>
                      [{ "time" => "2019-12-08 0:00:00", "temperature" => -1.0 },
                       { "time" => "2019-12-08 3:00:00", "temperature" => -1.0 },
                       { "time" => "2019-12-08 6:00:00", "temperature" => -2.0 },
                       { "time" => "2019-12-08 9:00:00", "temperature" => -2.0 },
                       { "time" => "2019-12-08 12:00:00", "temperature" => 0.0 },
                       { "time" => "2019-12-08 15:00:00", "temperature" => 1.0 },
                       { "time" => "2019-12-08 18:00:00", "temperature" => -1.0 },
                       { "time" => "2019-12-08 21:00:00", "temperature" => -2.0 }] }
      end

      it 'retutns errors' do
        expect(subject[:errors]).to include(a_kind_of(Errors::ImportError))
      end
    end

    context 'when location invalid' do
      let(:data) do
        { "source" => "rp5",
          "location" => "",
          "current_time" => "2019-12-07 08:19:45 +0600",
          "current_temp" => -1.0,
          "days" => 1,
          "prediction" =>
                      [{ "time" => "2019-12-08 0:00:00", "temperature" => -1.0 },
                       { "time" => "2019-12-08 3:00:00", "temperature" => -1.0 },
                       { "time" => "2019-12-08 6:00:00", "temperature" => -2.0 },
                       { "time" => "2019-12-08 9:00:00", "temperature" => -2.0 },
                       { "time" => "2019-12-08 12:00:00", "temperature" => 0.0 },
                       { "time" => "2019-12-08 15:00:00", "temperature" => 1.0 },
                       { "time" => "2019-12-08 18:00:00", "temperature" => -1.0 },
                       { "time" => "2019-12-08 21:00:00", "temperature" => -2.0 }] }
      end

      it 'retutns errors' do
        expect(subject[:errors]).to include(a_kind_of(Errors::ImportError))
      end
    end

    context 'without data' do
      let(:data) {}

      it 'retutns errors' do
        expect(subject[:errors]).to include(a_kind_of(Errors::ImportError))
      end
    end
  end
end
