require 'rails_helper'

describe FactualWeathers::Import do
  subject { described_class.call(location, time, temperature, wind_speed, wind_direction, humidity, pressure) }

  let(:location) { create(:location) }
  let(:time) { Faker::Time.backward(days: 5) }
  let(:temperature) { 4.5 }
  let(:wind_speed) { 3 }
  let(:wind_direction) { Faker::String.random(length: 1..3) }
  let(:humidity) { 95 }
  let(:pressure) { 1123 }

  context 'when temperature valid' do
    it 'returns Temperature#count' do
      expect { subject }.to change { FactualWeather.count }
    end

    it 'not to raise errors' do
      expect { subject }.not_to raise_error
    end
  end

  context 'when temperature invalid' do
    context 'when temperature is blank' do
      let(:temperature) {}

      it 'returns Temperature#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { FactualWeather.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'when time is blank' do
      let(:time) {}

      it 'returns Temperature#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { FactualWeather.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'without location' do
      let(:location) {}

      it 'returns Temperature#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { FactualWeather.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end
  end
end
