require 'rails_helper'

describe Temperatures::Import do
  subject { described_class.call(location, source, time, temperature, wind_speed, wind_direction, humidity, pressure, state, sunrise, sunset, felt_like) }

  let(:location) { create(:location) }
  let(:time) { Faker::Time.backward(days: 5) }
  let(:temperature) { 4.5 }
  let(:wind_speed) { 3 }
  let(:wind_direction) { Faker::String.random(length: 1..3) }
  let(:humidity) { 50 }
  let(:pressure) { 1121 }
  let(:source) { create(:source) }
  let(:state) { 'sunny' }
  let(:sunrise) { '07:00' }
  let(:sunset) { '07:00' }
  let(:felt_like) { 4.5 }

  context 'when temperature valid' do
    before do
      create(:factual_weather)
    end

    it 'returns Temperature#count' do
      expect { subject }.to change { Temperature.count }
    end

    it 'not to raise errors' do
      expect { subject }.not_to raise_error
    end
  end

  context 'when temperature invalid' do
    context 'when temperature is blank' do
      let(:temperature) {}

      it 'returns Temperature#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Temperature.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'when time is blank' do
      let(:time) {}

      it 'returns Temperature#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Temperature.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'without location' do
      let(:location) {}

      it 'returns Temperature#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Temperature.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end
  end
end
