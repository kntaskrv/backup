require 'rails_helper'

describe Locations::Import do
  subject { described_class.call(name, source) }

  let(:name) { 'location' }
  let(:source) { create(:source) }

  context 'when location valid' do
    it 'returns Location#count' do
      expect { subject }.to change { Location.count }
    end

    it 'not to raise errors' do
      expect { subject }.not_to(raise_error)
    end
  end

  context 'when location invalid' do
    context 'when name is blank' do
      let(:name) { '' }

      it 'returns Location#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Location.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'without name' do
      let(:name) {}

      it 'returns Location#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Location.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end
  end
end
