require 'rails_helper'

describe Predictions::Import do
  subject { described_class.call(predict, location, time, source, sunrise, sunset) }

  let(:predict) { { "time" => Time.now, "temperature" => 4.5, "wind_speed" => 3, "wind_direction" => "WSW", "days" => 5 } }
  let(:location) { create(:location) }
  let(:time) { Time.now }
  let(:source) { create(:source) }
  let(:sunrise) { '07:00' }
  let(:sunset) { '07:00' }

  context 'when prediction valid' do
    it 'returns Prediction#count' do
      expect { subject }.to change { Prediction.count }
    end

    it 'not to raise errors' do
      expect { subject }.not_to raise_error
    end
  end

  context 'when prediction invalid' do
    context 'when predict is empty' do
      let(:predict) { { time: nil, temperature: nil } }

      it 'returns Prediction#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Prediction.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context 'without predict' do
      let(:predict) {}

      it 'returns Prediction#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Prediction.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'without location' do
      let(:location) {}

      it 'returns Prediction#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Prediction.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'without days' do
      let(:predict) { { "time" => Time.now, "temperature" => 4.5, "wind_speed" => 3, "wind_direction" => "WSW" } }

      it 'returns Prediction#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Prediction.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context 'without time' do
      let(:time) {}

      it 'returns Prediction#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Prediction.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end
  end
end
