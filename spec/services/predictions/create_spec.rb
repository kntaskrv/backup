require 'rails_helper'

describe Predictions::Create do
  subject { described_class.call(city: city, datetime: datetime) }

  let(:datetime) { DateTime.new(2020, 2, 15, 10) }
  let(:prediction_time) { DateTime.new(2020, 2, 17, 12) }
  let!(:location) { create(:location, name: 'Omsk') }
  let(:city) { Cities::Omsk }
  let(:days) { 2 }

  context 'when sources have the same number of sticks and deviation' do
    before do
      gis = create(:source, name: 'Gismeteo')
      yan = create(:source, name: 'Yandex')
      bbc = create(:source, name: 'Bbc')
      hyd = create(:source, name: 'Hydromet')
      create(:source, name: 'Meteo-stat')
      create(:source, name: 'Meteo-stat(normal)')

      create(:prediction, source: gis, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -15)
      create(:prediction, source: yan, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -11)
      create(:prediction, source: bbc, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -17)
      create(:prediction, source: hyd, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -10)

      [gis, yan, bbc, hyd].each do |source|
        create(:global_rate, source: source, days: days, rate: 0.8)
        create(:average_deviation, source: source, days: days, average_deviation: 2)
      end
    end

    it 'returns our predictions' do
      expect(subject).to eq [{ "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 28,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 29,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 28,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 29,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 28,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 29,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 28,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 29,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 28,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 29,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 29,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil }]
    end
  end

  context 'when source have the same sticks and hydromet has deviation > 3' do
    before do
      gis = create(:source, name: 'Gismeteo')
      yan = create(:source, name: 'Yandex')
      bbc = create(:source, name: 'Bbc')
      hyd = create(:source, name: 'Hydromet')
      create(:source, name: 'Meteo-stat')
      create(:source, name: 'Meteo-stat(normal)')

      create(:prediction, source: gis, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -15)
      create(:prediction, source: yan, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -11)
      create(:prediction, source: bbc, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -17)
      create(:prediction, source: hyd, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -10)

      [gis, yan, bbc].each do |source|
        create(:global_rate, source: source, days: days, rate: 0.8)
        create(:average_deviation, source: source, days: days, average_deviation: 2)
      end
      create(:global_rate, source: hyd, days: days, rate: 0.8)
      create(:average_deviation, source: hyd, days: days, average_deviation: 5)
    end

    it 'returns our predictions' do
      expect(subject).to eq [{ "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 34,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 35,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 34,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 35,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 34,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 35,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 34,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 35,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 34,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 38,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 35,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil }]
    end
  end

  context 'when sources have the different number of sticks and the same deviation' do
    before do
      gis = create(:source, name: 'Gismeteo')
      yan = create(:source, name: 'Yandex')
      bbc = create(:source, name: 'Bbc')
      hyd = create(:source, name: 'Hydromet')
      create(:source, name: 'Meteo-stat')
      create(:source, name: 'Meteo-stat(normal)')

      create(:prediction, source: gis, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -15)
      create(:prediction, source: yan, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -11)
      create(:prediction, source: bbc, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -17)
      create(:prediction, source: hyd, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -10)

      [gis, yan, bbc, hyd].each do |source|
        create(:average_deviation, source: source, days: days, average_deviation: 2)
      end

      create(:global_rate, source: gis, days: days, rate: 0.7)
      create(:global_rate, source: yan, days: days, rate: 0.5)
      create(:global_rate, source: bbc, days: days, rate: 0.8)
      create(:global_rate, source: hyd, days: days, rate: 0.3)
    end

    it 'returns our predictions' do
      expect(subject).to eq [{ "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 40,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 41,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 40,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 41,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 40,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 41,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 40,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 41,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 40,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 47,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 41,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil }]
    end
  end

  context 'when source have the different sticks and hydromet has deviation > 3' do
    before do
      gis = create(:source, name: 'Gismeteo')
      yan = create(:source, name: 'Yandex')
      bbc = create(:source, name: 'Bbc')
      hyd = create(:source, name: 'Hydromet')
      create(:source, name: 'Meteo-stat')
      create(:source, name: 'Meteo-stat(normal)')

      create(:prediction, source: gis, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -15)
      create(:prediction, source: yan, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -11)
      create(:prediction, source: bbc, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -17)
      create(:prediction, source: hyd, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -10)

      create(:global_rate, source: gis, days: days, rate: 0.7)
      create(:global_rate, source: yan, days: days, rate: 0.5)
      create(:global_rate, source: bbc, days: days, rate: 0.8)
      create(:global_rate, source: hyd, days: days, rate: 0.3)
      create(:average_deviation, source: hyd, days: days, average_deviation: 5)
      [gis, yan, bbc].each do |source|
        create(:average_deviation, source: source, days: days, average_deviation: 2)
      end
    end

    it 'returns our predictions' do
      expect(subject).to eq [{ "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 46,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 2,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 12).to_time,
                               "source_id" => 47,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 46,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 12).to_time,
                               "source_id" => 47,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 46,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 12).to_time,
                               "source_id" => 47,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 46,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 20, 12).to_time,
                               "source_id" => 47,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 46,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -15.0,
                               "location_id" => 56,
                               "parsed_at" => DateTime.new(2020, 2, 15, 10).to_time,
                               "predicted_temperature" => -15.0,
                               "prediction_time" => DateTime.new(2020, 2, 21, 12).to_time,
                               "source_id" => 47,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil }]
    end
  end

  context 'data from remote server for days = 2' do
    let(:datetime) { DateTime.new(2020, 2, 13, 9) }
    let(:prediction_time) { DateTime.new(2020, 2, 15, 6) }

    before do
      gis = create(:source, name: 'Gismeteo')
      yan = create(:source, name: 'Yandex')
      bbc = create(:source, name: 'Bbc')
      hyd = create(:source, name: 'Hydromet')
      create(:source, name: 'Meteo-stat')
      create(:source, name: 'Meteo-stat(normal)')

      create(:prediction, source: gis, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -9)
      create(:prediction, source: yan, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -7)
      create(:prediction, source: bbc, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -9)
      create(:prediction, source: hyd, location: location, parsed_at: datetime, prediction_time: prediction_time, days: days, predicted_temperature: -5)

      create(:global_rate, source: gis, days: days, rate: 0.73)
      create(:global_rate, source: yan, days: days, rate: 0.68)
      create(:global_rate, source: bbc, days: days, rate: 0.72)
      create(:global_rate, source: hyd, days: days, rate: 0.33)
      create(:average_deviation, source: gis, days: days, average_deviation: 1.998)
      create(:average_deviation, source: yan, days: days, average_deviation: 2.499)
      create(:average_deviation, source: bbc, days: days, average_deviation: 2.143)
      create(:average_deviation, source: hyd, days: days, average_deviation: 4.634)
    end

    it 'returns our predictions' do
      expect(subject).to eq [{ "days" => 2,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 15, 6).to_time,
                               "source_id" => 52,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 2,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 15, 6).to_time,
                               "source_id" => 53,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 16, 6).to_time,
                               "source_id" => 52,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 3,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 16, 6).to_time,
                               "source_id" => 53,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 6).to_time,
                               "source_id" => 52,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 4,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 17, 6).to_time,
                               "source_id" => 53,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 6).to_time,
                               "source_id" => 52,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 5,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 18, 6).to_time,
                               "source_id" => 53,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 6).to_time,
                               "source_id" => 52,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil },
                             { "days" => 6,
                               "felt_like" => -9.0,
                               "location_id" => 65,
                               "parsed_at" => DateTime.new(2020, 2, 13, 9).to_time,
                               "predicted_temperature" => -9.0,
                               "prediction_time" => DateTime.new(2020, 2, 19, 6).to_time,
                               "source_id" => 53,
                               "state" => nil,
                               "sunrise" => nil,
                               "sunset" => nil }]
    end
  end
end
