require 'rails_helper'

describe Sources::Import do
  subject { described_class.call(name) }

  let(:name) { 'source' }

  context 'when source valid' do
    it 'returns Source#count' do
      expect { subject }.to change { Source.count }
    end

    it 'not to raise errors' do
      expect { subject }.not_to(raise_error)
    end
  end

  context 'when source invalid' do
    context 'when name is blank' do
      let(:name) { '' }

      it 'returns Source#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Source.count }
      end

      it 'raise errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end

    context 'without name' do
      let(:name) {}

      it 'returns Source#count' do
        expect do
          subject
        rescue StandardError
          nil
        end .not_to change { Source.count }
      end

      it 'returns errors' do
        expect { subject }.to raise_error(Errors::ImportError)
      end
    end
  end
end
