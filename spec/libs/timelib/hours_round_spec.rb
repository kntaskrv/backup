require 'rails_helper'

describe Timelib::HoursRound do
  include_examples 'hours rounder examples', Time.zone.today + 42.minutes, Time.zone.today + 1.hour
  include_examples 'hours rounder examples', Time.zone.today + 14.minutes, Time.zone.today

  include_examples 'hours rounder examples', Time.zone.today, Time.zone.today
  include_examples 'hours rounder examples', Time.now.end_of_day, Time.zone.tomorrow
end
