require 'rails_helper'

describe StickMethod::EqualDistribution do
  subject { described_class.call(array, sticks) }

  let(:array) { { 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0 } }
  let(:sticks) { 100 }

  context 'make equal distribution with good numbers' do
    it 'return hash with equal distribution' do
      expect(subject).to eq(1 => 20, 2 => 20, 3 => 20, 4 => 20, 5 => 20)
    end
  end

  context 'make equal distribution with bad numbers' do
    let(:sticks) { 72 }

    it 'return hash with equal distribution' do
      expect(subject).to eq(1 => 14, 2 => 14, 3 => 14, 4 => 14, 5 => 14)
    end
  end
end
