require 'rails_helper'

describe StickMethod::MathExpectation do
  subject { described_class.call(hash) }

  let(:hash) do
    { 0 => { -10 => 29, -9 => 29, -8 => 29, -7 => 93, -6 => 11, -5 => 11, -4 => 11, -13 => 18, -12 => 18, -11 => 18, -16 => 5, -15 => 5, -14 => 5 },
      12 => { -13 => 29, -12 => 29, -11 => 29, -10 => 111, -9 => 16, -8 => 16, -7 => 16, -15 => 13, -14 => 13, -6 => 5, -5 => 5 },
      18 => { -14 => 11, -13 => 11, -12 => 93, -11 => 11, -10 => 11, -9 => 11, -8 => 11, -21 => 13, -20 => 13, -19 => 13, -18 => 13, -17 => 13, -16 => 13 },
      6 => { -14 => 24, -13 => 24, -12 => 24, -11 => 24, -10 => 106, -9 => 11, -8 => 11, -15 => 13 } }
  end

  context 'make math expectation' do
    it 'return hash with math expectation' do
      expect(subject).to eq(0 => -9, 6 => -11, 12 => -11, 18 => -14)
    end
  end
end
