RSpec.shared_examples 'hours rounder examples' do |datetime, result|
  it 'rounds time to hours' do
    rounded_time = Timelib::HoursRound.call(datetime: datetime)

    expect(rounded_time).to eq result
  end
end
