class GetFactualWeatherWorker
  include Sidekiq::Worker

  def perform
    messages = BunnyClient::GetMessages.call(ENV['QUEUE_FACT_NAME'])
    messages.each do |message|
      ProcessFactualWeatherWorker.perform_async(message)
    end
  end
end
