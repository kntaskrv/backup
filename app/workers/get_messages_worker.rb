class GetMessagesWorker
  include Sidekiq::Worker

  def perform
    messages = BunnyClient::GetMessages.call(ENV['QUEUE_NAME'])
    messages.each do |message|
      ProcessMessageWorker.perform_async(message)
    end
  end
end
