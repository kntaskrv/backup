class ProcessFactualWeatherWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'import_factual'

  def perform(message)
    data = JSON.parse(message)
    ProcessFactualData.call(data)
  end
end
