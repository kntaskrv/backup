class ProcessOurPredictionWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'our_weathers'

  def perform
    Cities.constants. each do |city|
      Predictions::Create.call(city: Cities.const_get(city))
      Temperatures::Create.call(city: Cities.const_get(city))
    end
  end
end
