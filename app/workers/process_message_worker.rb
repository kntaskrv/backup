class ProcessMessageWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'import'

  def perform(message)
    data = JSON.parse(message)
    ProcessData.call(data)
  end
end
