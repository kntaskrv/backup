class Source < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many :rates
  has_many :temperatures
  has_many :locations, through: :rates
  has_many :predictions
  has_many :global_rates, dependent: :destroy
  has_many :average_deviations, dependent: :destroy

  scope :without_our, -> { where.not(name: %w[Meteo-stat Meteo-stat(normal)]) }
  scope :five_days, -> { all }
  scope :ours, -> { where(name: %w[Meteo-stat Meteo-stat(normal)]) }
end
