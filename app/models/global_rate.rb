class GlobalRate < ApplicationRecord
  validates :days, :true_strike, :strike_count, presence: true
  validates :source_id, uniqueness: { scope: %i[days location_id] }

  belongs_to :source
  belongs_to :location
end
