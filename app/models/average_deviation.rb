class AverageDeviation < ApplicationRecord
  validates :days, :average, :count, presence: true
  validates :source_id, uniqueness: { scope: %i[days location_id] }

  belongs_to :source
  belongs_to :location
end
