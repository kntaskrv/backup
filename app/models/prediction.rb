class Prediction < ApplicationRecord
  validates :days, :parsed_at, :prediction_time, :predicted_temperature, presence: true

  belongs_to :location
  belongs_to :source

  scope :by_period, ->(from, to) { where('parsed_at >= :from AND parsed_at <= :to', from: from, to: to) }
end
