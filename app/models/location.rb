class Location < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  has_many :rates
  has_many :sources, through: :rates
  has_many :factual_weathers, dependent: :destroy
  has_many :temperatures, dependent: :destroy
  has_many :predictions, dependent: :destroy
  has_many :global_rates, dependent: :destroy
  has_many :average_deviations, dependent: :destroy
end
