class Temperature < ApplicationRecord
  validates :parsed_at, :temperature, presence: true

  belongs_to :location
  belongs_to :source

  scope :by_period, ->(from, to) { where('parsed_at >= :from AND parsed_at <= :to', from: from, to: to) }
end
