module ApplicationHelper
  def day_name(day_number)
    return t('headers.today') if day_number.to_i.zero?

    day_number.to_date.strftime('%d %B')
  end
end
