module AverageDeviations
  class Calculate < BaseService
    param :source
    param :days
    param :deviation
    param :count
    param :location

    def call
      calculate_average_deviation
    end

    private

    def calculate_average_deviation
      average_deviation.average += deviation
      average_deviation.count += count
      average_deviation.average_deviation = average_deviation.average / average_deviation.count.to_f
      average_deviation.save
    end

    def average_deviation
      @average_deviation ||= AverageDeviation.find_or_initialize_by(
        source_id: source.id,
        days: days,
        location_id: location.id
      )
    end
  end
end
