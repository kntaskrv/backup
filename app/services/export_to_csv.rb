require 'csv'

class ExportToCsv < BaseService
  option :file_name, default: -> { 'check_data.csv' }
  option :data, default: -> { [] }

  def call
    CSV.open(file_name, 'ab') do |csv|
      csv << data
    end
  end
end
