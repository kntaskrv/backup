module Logs
  class Filter < BaseService
    param :log_name
    param :filter_type, optional: true

    def call
      messages = load_messages
      messages = filter(messages) if filter_type.present?
      messages
    end

    private

    def load_messages
      File.open(ENV['PATH_TO_PARSERS_LOG'] + "/" + log_name + '.log') { |file| file&.read }.split("\n")
    end

    def filter(messages)
      messages = messages.map { |message| message if message.slice(1..4).include?(filter_type) }.compact
      messages
    end
  end
end
