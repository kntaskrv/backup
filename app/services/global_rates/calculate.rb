module GlobalRates
  class Calculate < BaseService
    param :source
    param :days
    param :true_strikes
    param :count
    param :location

    def call
      calculate_global_rate
    end

    private

    def calculate_global_rate
      global_rate.true_strike += true_strikes
      global_rate.strike_count += count
      global_rate.rate = (global_rate.strike_count.zero? ? 0 : global_rate.true_strike / global_rate.strike_count.to_f)
      global_rate.save
    end

    def global_rate
      @global_rate ||= GlobalRate.find_or_initialize_by(
        source_id: source.id,
        days: days,
        location_id: location.id
      )
    end
  end
end
