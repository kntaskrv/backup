class ProcessData < BaseService
  param :data

  def call
    self.result = {
      errors: []
    }
    validate!

    source = Sources::Import.call(data[:source])
    location = Locations::Import.call(data[:location])
    import_temperature(location, source) if data[:current_temp].present?
    predictions(location, source)
    result
  rescue Errors::ImportError, ActiveRecord::RecordInvalid, StandardError => e
    my_logger.warn(e)
    result[:errors] << e
    result
  end

  private

  def my_logger
    @my_logger ||= Logger.new(Rails.root.join('log/import.log'))
  end

  def validate!
    raise Errors::ImportError, "data can't be blank" if data.blank?
  end

  def import_temperature(location, source)
    Temperatures::Import.call(
      location, source,
      data[:current_time],
      data[:current_temp],
      data[:wind_speed],
      data[:wind_direction],
      data[:humidity],
      data[:pressure],
      data[:state],
      data[:sunrise],
      data[:sunset],
      data[:felt_like]
    )
  end

  def predictions(location, source)
    time = data[:current_time]
    sunrise = data[:sunrise]
    sunset = data[:sunset]
    data[:prediction].each do |predict|
      Predictions::Import.call(predict, location, time, source, sunrise, sunset)
    end
  end
end
