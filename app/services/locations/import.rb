module Locations
  class Import < BaseService
    param :name

    def call
      validate!

      location.save!
      self.result = location
      result
    end

    private

    def validate!
      raise Errors::ImportError, 'name can\'t be blank' if name.blank?
    end

    def location
      @location ||= Location.find_or_initialize_by(name: name)
    end
  end
end
