class BaseService
  extend Dry::Initializer

  DEFAULT_LOCATION = Location.find_by(name: 'Omsk')
  OUR_SOURCE = Source.find_by(name: 'Meteo-stat')
  OUR_SOURCE_NORMAL = Source.find_by(name: 'Meteo-stat(normal)')
  BASE_COUNTRY = 'Russian Federation'.freeze

  attr_accessor :result

  class << self
    def call(*args, &block)
      new(*args).call(&block)
    end

    def new(*args)
      args << args.pop.symbolize_keys if args.last.is_a?(Hash)
      super(*args)
    end
  end
end
