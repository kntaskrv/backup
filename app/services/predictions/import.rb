module Predictions
  class Import < BaseService
    param :predict
    param :location
    param :time
    param :source
    param :sunrise
    param :sunset

    def call
      validate!
      prediction.save!
      self.result = prediction
      result
    end

    private

    def validate!
      raise Errors::ImportError, 'location can\'t be blank' if location.blank?
      raise Errors::ImportError, 'predict can\'t be blank' if predict.blank?
      raise Errors::ImportError, 'time can\'t be blank' if time.blank?
      raise prediction.validate! if prediction.invalid?
    end

    def prediction
      @prediction ||= Prediction.find_or_initialize_by(
        location: location,
        source: source,
        sunrise: sunrise,
        sunset: sunset,
        state: predict["state"],
        prediction_time: predict["time"],
        predicted_temperature: predict["temperature"],
        felt_like: predict["felt_like"],
        wind_speed: predict["wind_speed"],
        wind_direction: predict["wind_direction"],
        days: predict["days"],
        parsed_at: time,
        humidity: predict["humidity"],
        pressure: predict["pressure"],
        precipitation: predict["precipitation"]
      )
    end
  end
end
