module Predictions
  class Create < BaseService
    option :days, default: -> { 6 }
    option :city
    option :datetime, default: -> { current_time_in(city) }
    option :equal_distributions, default: -> { { 0 => [], 6 => [], 12 => [], 18 => [] } }
    option :normal_distributions, default: -> { { 0 => [], 6 => [], 12 => [], 18 => [] } }
    option :offset, default: -> { city.timezone_offset.to_i % 6 }

    def call
      sources = Source.without_our
      @result = []
      (0..days).each do |days|
        arr_pred = []
        @days = days
        sources.each do |source|
          @source = source
          next if deviation > 3
          predictions.each do |prediction|
            arr_pred << prediction
            @prediction = prediction
            calc_distributions
          end
        end
        @grouped_weather = arr_pred.group_by { |i| i.prediction_time.hour % 24 }
        create_predictions
      end
      result
    end

    private

    attr_reader :days, :source, :result, :prediction, :grouped_weather

    def average_weather
      grouped_weather.each_with_object({}) do |(hour, weather), result|
        winds = weather.map { |i| i.wind_speed }.compact
        pressures = weather.map { |i| i.pressure }.compact
        humidites = weather.map { |i| i.humidity }.compact
        states = weather.map { |i| i.state }.compact
        state = Forecasts::Calculate.call(states)
        result[(hour + offset) % 24] = {
          wind_speed: winds.sum / winds.size.to_f,
          pressure: pressures.sum / pressures.size.to_f,
          humidity: humidites.sum / humidites.size.to_f,
          state: state
        }
      end
    end

    def load_states
      grouped_weather.each_with_object({}) do |(hour, weather), result|
        result[hour] = weather.map(&:state)
      end
    end

    def calc_distributions
      equal_distributions[time_of(prediction).hour] << equal_distribution
      normal_distributions[time_of(prediction).hour] << normal_distribution
    end

    def location
      @location ||= Location.find_by(name: city.name)
    end

    def time_of(prediction)
      (prediction.prediction_time + city.timezone_offset.to_i.hour)
    end

    def create_predictions
      our_distributions = StickMethod::MergeDistribution.call(equal_distributions)
      our_predictions = StickMethod::MathExpectation.call(our_distributions)
      import_predictions(our_predictions, Source.find_by(name: 'Meteo-stat'))

      our_distributions = StickMethod::MergeDistribution.call(normal_distributions)
      our_predictions = StickMethod::MathExpectation.call(our_distributions)
      import_predictions(our_predictions, Source.find_by(name: 'Meteo-stat(normal)'))
    end

    def import_predictions(predictions, source)
      # binding.pry
      predictions = predictions.select { |hour, temp| hour == average_weather.keys.last } if days == 6
      predictions.each do |hour, temperature|
        date = datetime.to_date + days
        timestamp = DateTime.new(date.year, date.month, date.day, hour, 0, 0, city.timezone_offset)
        prediction = Prediction.new(
          location: location,
          source: source,
          prediction_time: timestamp,
          predicted_temperature: temperature,
          felt_like: temperature,
          wind_speed: average_weather[hour][:wind_speed],
          wind_direction: nil,
          days: (timestamp.to_date - datetime.to_date).to_i,
          parsed_at: datetime,
          humidity: average_weather[hour][:humidity],
          pressure: average_weather[hour][:pressure],
          precipitation: nil,
          state: average_weather[hour][:state],
          sunrise: nil,
          sunset: nil
        )
        prediction.save!
        result << prediction.attributes.except("id", "updated_at", "created_at", "wind_direction", "wind_speed", "temperature_id", "pressure", "precipitation", "humidity", "difference")
    end
    end

    def current_time_in(city)
      @current_time_in_city ||= Timelib::Detector.new(city.timezone_offset).call
    end

    def equal_distribution
      StickMethod::EqualDistribution.call(interval, sticks)
    end

    def normal_distribution
      StickMethod::NormalDistribution.call(interval, sticks)
    end

    def interval
      left = (prediction.predicted_temperature - deviation).round
      right = (prediction.predicted_temperature + deviation).round
      (left..right).map { |i| [i, 0] }.to_h
    end

    def sticks
      stick = (source.global_rates.where(days: days, location_id: location.id).last&.rate.to_f * 100).to_i
      stick.zero? ? 100 : stick
    end

    def deviation
      source&.average_deviations&.where(days: days, location_id: location.id)&.last&.average_deviation || 1.5
    end

    def predictions
      offset = city.timezone_offset.to_i % 6
      prediction_hours = [6, 12, 18, 0].map { |hour| (hour - offset) % 24 }
      date = datetime
      source.predictions
            .where(days: days, location: location)
            .by_period(date.beginning_of_day, date.end_of_day)
            .select { |p| (p.parsed_at - date).abs < 1800 && prediction_hours.include?(p.prediction_time.hour) }
    end
  end
end
