module Temperatures
  class Create < BaseService
    option :city
    option :source, default: -> { OUR_SOURCE }
    option :weather, default: -> { location.temperatures.where(source_id: Source.find_by(name: 'Gismeteo')).last }
    option :days, default: -> { 0 }
    option :equal_distributions, default: -> { { current_hour => [] } }
    option :normal_distributions, default: -> { { current_hour => [] } }

    def call
      validate!

      @result = []
      sources = Source.without_our
      sources.each do |source|
        @source = source
        calc_distibutions
      end
      create_temperature
      result
    end

    private

    attr_reader :source, :result

    def calc_distibutions
      equal_distributions[current_hour] << equal_distribution
      normal_distributions[current_hour] << normal_distribution
    end

    def load_temperature
      Temperature.where(source_id: source.id, location_id: location.id).last
    end

    def current_hour(time = Time.now)
      @current_hour ||= time.hour
    end

    def create_temperature
      our_distributions = StickMethod::MergeDistribution.call(equal_distributions)
      our_temperature = StickMethod::MathExpectation.call(our_distributions)
      import_temperature(our_temperature, OUR_SOURCE)

      our_distributions = StickMethod::MergeDistribution.call(normal_distributions)
      our_temperature = StickMethod::MathExpectation.call(our_distributions)
      import_temperature(our_temperature, OUR_SOURCE_NORMAL)
    end

    def sticks
      stick = (source.global_rates.where(days: days, location_id: location.id).last&.rate.to_f * 100).to_i
      stick.zero? ? 100 : stick
    end

    def equal_distribution
      StickMethod::EqualDistribution.call(interval, sticks)
    end

    def normal_distribution
      StickMethod::NormalDistribution.call(interval, sticks)
    end

    def deviation
      source&.average_deviations&.where(days: days, location_id: location.id)&.last&.average_deviation || 1.5
    end

    def interval
      left = (load_temperature.temperature - deviation).round
      right = (load_temperature.temperature + deviation).round
      (left..right).map { |i| [i, 0] }.to_h
    end

    def location
      @location ||= Location.find_by(name: city.name)
    end

    def validate!
      raise Errors::ImportError, 'weather can\'t be blank' if weather.blank?
    end

    def import_temperature(temperature, source)
      temperature = Temperature.find_or_initialize_by(
        location: location,
        source: source,
        parsed_at: weather.parsed_at,
        temperature: temperature.values.last,
        wind_speed: weather.wind_speed,
        wind_direction: weather.wind_direction,
        humidity: weather.humidity,
        pressure: weather.pressure,
        state: weather.state,
        sunrise: weather.sunrise,
        sunset: weather.sunset,
        felt_like: weather.felt_like
      )
      temperature.save
      Rates::MeasurementErrors::Create.call(weather_measure_record: temperature)
      result << temperature
    end
  end
end
