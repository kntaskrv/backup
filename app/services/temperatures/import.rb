module Temperatures
  class Import < BaseService
    param :location
    param :source
    param :time
    param :temp
    param :wind_speed
    param :wind_direction
    param :humidity
    param :pressure
    param :state
    param :sunrise
    param :sunset
    param :felt_like

    def call
      validate!

      temperature.save!
      self.result = temperature
      Rates::MeasurementErrors::Create.call(weather_measure_record: temperature)
      result
    end

    private

    def validate!
      raise Errors::ImportError, 'location can\'t be blank' if location.blank?
      raise Errors::ImportError, 'time can\'t be blank' if time.blank?
      raise Errors::ImportError, 'temperature can\'t be blank' if temp.blank?
    end

    def temperature
      @temperature ||= Temperature.find_or_initialize_by(
        location: location,
        source: source,
        parsed_at: time,
        temperature: temp,
        wind_speed: wind_speed,
        wind_direction: wind_direction,
        humidity: humidity,
        pressure: pressure,
        state: state,
        sunrise: sunrise,
        sunset: sunset,
        felt_like: felt_like
      )
    end
  end
end
