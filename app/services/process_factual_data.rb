class ProcessFactualData < BaseService
  param :data

  def call
    self.result = {
      errors: []
    }
    validate!

    location = Locations::Import.call(data[:location])
    FactualWeathers::Import.call(location, data[:current_time], data[:current_temp], data[:wind_speed], data[:wind_direction], data[:humidity], data[:pressure])
    result
  rescue Errors::ImportError, ActiveRecord::RecordInvalid => e
    result[:errors] << e
    result
  end

  private

  def validate!
    raise Errors::ImportError, "data can't be blank" if data.blank?
  end
end
