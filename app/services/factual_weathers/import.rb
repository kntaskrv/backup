module FactualWeathers
  class Import < BaseService
    param :location
    param :time
    param :temp
    param :wind_speed
    param :wind_direction
    param :humidity
    param :pressure

    def call
      validate!

      factual_weather.save!
      self.result = factual_weather
      result
    end

    private

    def validate!
      raise Errors::ImportError, 'location can\'t be blank' if location.blank?
      raise Errors::ImportError, 'time can\'t be blank' if time.blank?
      raise Errors::ImportError, 'temperature can\'t be blank' if temp.blank?
    end

    def factual_weather
      @factual_weather ||= FactualWeather.find_or_initialize_by(
        location: location,
        parsed_at: time,
        temperature: temp,
        wind_speed: wind_speed,
        wind_direction: wind_direction,
        humidity: humidity,
        pressure: pressure
      )
    end
  end
end
