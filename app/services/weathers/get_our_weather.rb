module Weathers
  class GetOurWeather < BaseService
    option :location
    option :for_days, default: -> { 5 }

    def call
      # Returns forecast in hash like (key - day, (key - hour stamps (0, 3, 6 ,9 etc), value - temperature at this timestamp)):
      #  {'0'=>{ 'gismeteo' => { '0'=>-9.15, '3'=>-3.11, '6'=>-8.64, '9'=>-9.84, '12'=>-10.42, '15'=>-1.3, '18'=>-8.51, '21'=>-2.55 }},
      #  ...
      #  '5'=>{ 'yandex' => { '0'=>-12.12, '3'=>-6.66, '6'=>-13.0, '9'=>-3.36, '12'=>-2.95, '15'=>-5.51, '18'=>-6.98, '21'=>-6.12}}}
      today_forecast.merge(future_forecast).stringify_keys
    end

    private

    attr_reader :sunset, :sunrise

    def today_forecast
      source = best_source_for(0)
      weathers = select_predictions_for(0, source)
      { Date.today => weathers.merge(source: source&.name).merge(average_indications(weathers)) }
    end

    def future_forecast
      # Forecasst for future five days (not counting today)

      @forecast ||= (1..for_days).map do |days_ago_count|
        source = best_source_for(days_ago_count)
        weathers = select_predictions_for(days_ago_count, source)
        [Date.today + days_ago_count, weathers.merge(source: source&.name).merge(average_indications(weathers))]
      end.to_h
    end

    def average_indications(weathers)
      count = weathers.values.count
      result = {
        average_temperature: 0,
        average_wind_speed: 0,
        average_humidity: 0,
        average_pressure: 0,
        average_state: 'sunny',
        sunrise: sunrise || '06:00',
        sunset: sunset || '20:20'
      }
      directions = []
      states = weathers.values.map { |weather| weather[:state] }
      weathers.values.each do |weather|
        result[:average_temperature] += (weather[:temperature] / count).round.to_f
        result[:average_wind_speed] += (weather[:wind_speed] / count).round.to_f
        result[:average_humidity] += (weather[:humidity] / count).round.to_f
        result[:average_pressure] += (weather[:pressure] / count).round.to_f
        result[:average_state] = Forecasts::Calculate.call(states)
        directions += weather[:wind_direction].split('')
      end
      result[:average_wind_direction] = AverageDirection::Calculate.call(directions)
      round_indications(result)
    end

    def round_indications(indications)
      {
        average_temperature: indications[:average_temperature].round.to_f,
        average_wind_speed: indications[:average_wind_speed].round.to_f,
        average_wind_direction: indications[:average_wind_direction],
        average_humidity: indications[:average_humidity].round.to_f,
        average_pressure: indications[:average_pressure]&.round&.to_f,
        average_state: indications[:average_state],
        sunrise: sunrise || '06:00',
        sunset: sunset || '20:20'
      }
    end

    def beginning_of_today
      @beginning_of_today ||= Time.zone.today.beginning_of_day
    end

    def analyze_hours
      @analyze_hours ||= Rails.configuration.application.dig(:analyze_hours)
    end

    def select_predictions_for(day_ago_count, source)
      # Finds last predictions for the day (for example: day_ago_count = 1 for tomorrow)
      offset = Cities.const_get(location.name.underscore.camelcase).timezone_offset.to_i
      analyze_hours.each_with_object({}) do |hours_count, result|
        datetime = beginning_of_today + day_ago_count.days + hours_count.hours - offset.hours
        datetime += 1.day if [0, 3].include?(hours_count)
        last_prediction = predictions.where(source: source, prediction_time: datetime).last
        last_prediction = predictions.where(source: source).last if last_prediction.nil?
        result.merge!(build_data(last_prediction, hours_count))
      end
    end

    def build_data(last_prediction, hours_count)
      @sunset = last_prediction&.sunset
      @sunrise = last_prediction&.sunrise
      {
        hours_count => {
          temperature: last_prediction&.predicted_temperature.round.to_f,
          felt_like: last_prediction&.predicted_temperature.round.to_f,
          wind_speed: last_prediction&.wind_speed.round.to_f || 3,
          wind_direction: last_prediction&.wind_direction || 'SW',
          humidity: last_prediction&.humidity.round.to_f || 76,
          pressure: last_prediction&.pressure.round.to_f || 762,
          state: last_prediction&.state || 'sunny'
        }
      }
    end

    def location_predictions
      @location_predictions ||= location.predictions
    end

    def predictions
      @predictions ||= location_predictions.by_period(Date.today, Time.now).order(:prediction_time)
    end

    def best_source_for(day_ago_count)
      source_id = GlobalRate.where(location_id: location.id, days: day_ago_count, source_id: Source.ours.ids).select(:source_id, :rate).order(rate: :desc).first&.source_id

      @source = Source.find_by(id: source_id) || Source.ours.take
    end
  end
end
