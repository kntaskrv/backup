module Weathers
  class DetectBestSource < BaseService
    option :days, default: -> { 0 }
    option :location

    def call
      source
    end

    def source
      source_id = GlobalRate.where(days: days, location_id: location.id).select(:source_id, :rate).order(rate: :desc).first&.source_id
      Source.find_by(id: source_id) || Source.take
    end
  end
end
