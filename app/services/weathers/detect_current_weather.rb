module Weathers
  class DetectCurrentWeather < BaseService
    option :days, default: -> { 0 }
    option :location, default: -> { Location.take }

    STATES = %w[sunny cloudy rain snow clear].freeze

    def call
      current_weather
    end

    def current_weather
      weather = source.temperatures.where(location_id: location.id).last
      @current_weather = {
        source: source.name,
        city: weather.location.name,
        date: weather.parsed_at,
        felt_like: weather&.felt_like || weather&.temperature,
        humidity: weather.humidity,
        country: BASE_COUNTRY,
        pressure: weather.pressure,
        state: Forecasts::Calculate.call([weather.state]) || STATES.sample,
        temperature: weather.temperature,
        wind_direction: weather.wind_direction,
        wind_speed: weather.wind_speed
      }
    end

    def source
      return @source if @source.present?

      source_id = GlobalRate.where(days: 0, location_id: location.id).select(:source_id, :rate).order(rate: :desc).first&.source_id
      @source = Source.find_by(id: source_id) || Source.take
    end
  end
end
