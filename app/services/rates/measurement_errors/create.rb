module Rates
  module MeasurementErrors
    class Create < BaseService
      option :weather_measure_record # Temperature instance for now
      option :measure_name, default: -> { weather_measure_record.class.name.underscore }
      option :days_back, default: -> { 5 }
      option :factual_weather, default: -> { location.factual_weathers.last }

      attr_reader :measurement_errors

      def call
        calc_rate_for_today
        calc_average_deviation_for_today
        return nil unless data?

        days = days_back
        days.times do |i|
          prediction_arr = suitable_predictions(i + 1)
          true_strikes = 0
          dev = 0
          count = prediction_arr.size
          next if count.zero?

          prediction_arr.each do |p|
            true_strikes += 1 if good_error?(p.predicted_temperature)
            dev += deviation(p.predicted_temperature)
          end
          GlobalRates::Calculate.call(source, i + 1, true_strikes, count, location)
          AverageDeviations::Calculate.call(source, i + 1, dev, count, location)
        end
      end

      private

      def calc_average_deviation_for_today
        AverageDeviations::Calculate.call(source, 0, deviation(weather_measure_record.temperature), 1, location)
      end

      def calc_rate_for_today
        good_error?(weather_measure_record.temperature) ? GlobalRates::Calculate.call(source, 0, 1, 1, location) : GlobalRates::Calculate.call(source, 0, 0, 1, location)
      end

      def calc_global_rate(global_rate)
        return 0 if global_rate.strike_count == 0

        global_rate.rate = global_rate.true_strike / global_rate.strike_count.to_f
      end

      def good_error?(temperature)
        temp_float?(temperature) ? deviation(temperature) < 3.5 : deviation(temperature) < 3
      end

      def temp_float?(temperature)
        temperature.to_i < temperature
      end

      def deviation(temperature)
        if factual_weather.present?
          (temperature - factual_weather.temperature).abs
        else
          1.5
        end
      end

      def get_global_rate(days)
        GlobalRate.find_or_initialize_by(
          source_id: source.id,
          days: days,
          location_id: location.id
        )
      end

      def analyze_hours
        Rails.configuration.application.dig(:analyze_hours)
      end

      def data?
        analyzable_measure?
      end

      def analyzable_measure?
        analyze_hours.include? rounded_parsed_time.strftime('%H').to_i
      end

      def suitable_predictions(days)
        location.predictions.where(source: source, days: days, prediction_time: rounded_parsed_time - offset.hours)
      end

      def rounded_parsed_time
        @rounded_parsed_time ||= Timelib::HoursRound.call(datetime: weather_measure_record.parsed_at)
      end

      def offset
        Cities.const_get(location.name.underscore.camelcase).timezone_offset.to_i % 6
      end

      def source
        @source ||= weather_measure_record.source
      end

      def location
        @location ||= weather_measure_record.location
      end
    end
  end
end
