module Rates
  class Load < BaseService
    param :location

    def call
      Source.joins(:global_rates)
            .select(:id, :name, :days, :rate, :true_strike, :strike_count, :location_id)
            .where(global_rates: { location_id: location.id })
            .order(:id, :days)
            .each_with_object({}) do |rate, result|
              result[rate.name] = result[rate.name] || {}
              result[rate.name][rate.days] = {
                rate: rate.rate,
                true_strike: rate.true_strike,
                strike: rate.strike_count
              }
            end
    end
  end
end
