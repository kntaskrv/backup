module Sources
  class Import < BaseService
    param :name

    def call
      validate!

      source.save!
      self.result = source
      result
    end

    private

    def validate!
      raise Errors::ImportError, 'source can\'t be blank' if name.blank?
    end

    def source
      @source ||= Source.find_or_initialize_by(name: name)
    end
  end
end
