module Forecasts
  class Calculate < BaseService
    param :states

    STATES = { 
      'clear sky' => 0,
      'sunny' => 0,
      'clear' => 0,
      'fair' => 0,
      'mostly clear' => 0,
      'sunny intervals' => 1,
      'light cloud' => 1,
      'mainly cloudy' => 1,
      'partly' => 1,
      'intervals' => 1,
      'cloudy' => 1,
      'overcast' => 2,
      'overcast and light rain' => 2,
      'partly cloudy and light rain' => 2,
      'thick cloud' => 2,
      'windy and overcast' => 3,
      'light rain' => 3,
      'Mainly cloudy, light snow' => 3,
      'mainly cloudy, snow' => 3,
      'partly cloudy and showers' => 3,
      'light rain showers' => 3,
      'rain' => 3,
      'Heavy Rain' => 3,
      'Overcast and showers' => 3,
      'hail showers' => 3,
      'Sleet Showers' => 3,
      'mainly cloudy, rain' => 3,
      'mainly cloudy, very heavy rain' => 3,
      'mainly cloudy, mixture of rain and snow' => 3,
      'partly cloudy, light rain, thunderstorm' => 4,
      'mainly cloudy, light rain, thunderstorm' => 4,
    }
    OUR_STATES = { 0 => 'sunny', 1 => 'cloudy', 2 => 'overcast', 3 => 'rain', 4 => 'storm' }
    def call
      sum = states.map { |state| STATES[state&.downcase] }.compact
      sum = sum.sum / sum.size.to_f if sum.size.positive?
      sum = 0 if sum.nil? || sum.class == Array
      OUR_STATES[sum.round]
    end
  end
end