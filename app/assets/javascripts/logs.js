let name = ''

$(document).on('click', '#infoCheck', function (e){
  let info = $("#infoCheck").prop("checked")
  let warn = $("#warnCheck").prop("checked")
  if (info && warn) {
    getData()
  } else if(info) {
    getData({filter: 'INFO'})
  } else if(warn) {
    getData({filter: 'WARN'})
  } else {
    getData({}, name)
  }
});

$(document).on('click', '#warnCheck', function (e){
  let info = $("#infoCheck").prop("checked")
  let warn = $("#warnCheck").prop("checked")
  if (warn && info) {
    getData()
  } else if(warn) {
    getData({filter: 'WARN'})
  } else if(info) {
    getData({filter: 'INFO'})
  } else {
    getData()
  }
});

$(document).on('click', '#log_rp5', function (e){
  name = 'rp5'
  $.ajax({
    url: '/logs/open?name=rp5',
    dataType: 'script',
    data: {name: 'rp5'},
  })
});

$(document).on('click', '#log_darksky', function (e){
  name = 'darksky'
  $.ajax({
    url: '/logs/open?name=darksky',
    dataType: 'script',
    data: {name: 'darksky'},
  })
});

$(document).on('click', '#log_foreca', function (e){
  name = 'foreca'
  $.ajax({
    url: '/logs/open?name=foreca',
    dataType: 'script',
    data: {name: 'foreca'},
  })
});

$(document).on('click', '#log_gismeteo', function (e){
  name = 'gismeteo'
  $.ajax({
    url: '/logs/open?name=gismeteo',
    dataType: 'script',
    data: {name: 'gismeteo'},
  })
});

$(document).on('click', '#log_yandex', function (e){
  name = 'yandex'
  $.ajax({
    url: '/logs/open?name=yandex',
    dataType: 'script',
    data: {name: 'yandex'},
  })
});

$(document).on('click', '#log_accuweather', function (e){
  name = 'accuweather'
  $.ajax({
    url: '/logs/open?name=accuweather',
    dataType: 'script',
    data: {name: 'accuweather'},
  })
});

$(document).on('click', '#log_bbc', function (e){
  name = 'bbc'
  $.ajax({
    url: '/logs/open?name=bbc',
    dataType: 'script',
    data: {name: 'bbc'},
  })
});

$(document).on('click', '#log_hydromet', function (e){
  name = 'hydromet'
  $.ajax({
    url: '/logs/open?name=hydromet',
    dataType: 'script',
    data: {name: 'hydromet'},
  })
});

let getData = (data = {}) => {
  $.ajax({
    url: '/logs/open?name=' + name,
    dataType: 'script',
    data,
  })
}