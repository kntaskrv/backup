class RatesController < ApplicationController
  def index
    @best_source = Weathers::DetectBestSource.call(location: location)
    @weathers = Weathers::Detect.call(location: location)
    @sources = Source.five_days
    @our_weathers = Weathers::GetOurWeather.call(location: location)
  end

  private

  def location
    @location ||= Location.find_by(name: params[:name].capitalize) || Location.take
  end
end
