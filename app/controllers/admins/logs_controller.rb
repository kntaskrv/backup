module Admins
  class LogsController < ApplicationController
    before_action :authenticate_admin!

    def index
      logs
    end

    def open
      @messages = Logs::Filter.call(params[:name], params[:filter])

      respond_to do |format|
        format.html { render 'open.html.erb', name: params[:name] }
        format.js {}
      end
    end

    def logs
      @logs ||= Dir.open(ENV['PATH_TO_PARSERS_LOG']).to_a.map { |item| item.split('.').first if item.match?(/\.log$/) }.compact
    end
  end
end
