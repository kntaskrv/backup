module Api
  module V1
    class CitiesController < ApplicationController
      api :GET, '/v1/cities', 'Get array of cities'
      returns code: 200, desc: 'array of cities'
      def take_all
        render json: Location.select(:id, :name).pluck(:name)
      end
    end
  end
end
