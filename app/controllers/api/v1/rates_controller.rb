module Api
  module V1
    class RatesController < ApplicationController
      api :GET, '/v1/rates/current_weather/:name', 'Get current weather from best source of today'
      param :name, String, desc: 'City name'
      returns code: 200, desc: 'current weathers' do
        property :temperature, Float, desc: 'current temperature'
        property :wind_speed, Float, desc: 'current wind speed'
        property :wind_direction, String, desc: 'current wind direction'
        property :humidity, Float, desc: 'current humidity'
        property :pressure, Float, desc: 'current pressure'
        property :source, String, desc: 'name of source'
      end

      def current_weather
        render json: Weathers::DetectCurrentWeather.call(location: location)
      end

      api :GET, '/v1/rates/weathers/:name', 'Get predicted weathers from other sources'
      param :name, String, desc: 'City name'
      returns code: 200, desc: 'hash of predicted weathers from other sources' do
        property :weathers, Hash do
          property :weather, Hash do
            property :temperature, Float, desc: 'predicted temperature'
          end
        end
      end

      def weathers
        render json: Weathers::Detect.call(location: location)
      end

      api :GET, '/v1/rates/our_weathers/:name', 'Get predicted weathers from our sources'
      param :name, String, desc: 'City name'
      returns code: 200, desc: 'hash of predicted weathers from our sources' do
        property :weathers, Hash do
          property :weather, Hash do
            property :temperature, Float, desc: 'predicted temperature'
          end
        end
      end
      def our_weathers
        render json: Weathers::GetOurWeather.call(location: location)
      end

      api :GET, '/v1/rates/global_rates/:name', 'Get rates for sources and location'
      param :name, String, desc: 'City name'
      returns code: 200, desc: 'hash of global rates for each source' do
        property :rates, Hash do
          property :source_rates, Hash do
            property :rate, Float, desc: 'rate of source'
          end
        end
      end
      def global_rates
        render json: Rates::Load.call(location)
      end

      private

      def location
        @location ||= Location.find_by(name: params[:name].capitalize) || Location.take
      end
    end
  end
end
