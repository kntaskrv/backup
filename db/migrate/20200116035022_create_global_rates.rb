class CreateGlobalRates < ActiveRecord::Migration[6.0]
  def change
    create_table :global_rates do |t|
      t.bigint :source_id, null: false
      t.integer :days, null: false
      t.integer :true_strike, null: false, default: 0
      t.integer :strike_count, null: false, default: 0
      t.float :rate

      t.timestamps

      t.index [:source_id, :days], unique: true
    end
  end
end
