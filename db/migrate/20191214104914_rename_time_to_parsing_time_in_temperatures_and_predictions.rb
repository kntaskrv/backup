class RenameTimeToParsingTimeInTemperaturesAndPredictions < ActiveRecord::Migration[6.0]
  def up
    rename_column :temperatures, :time, :parsed_at
    rename_column :predictions, :time, :parsed_at
  end

  def down
    rename_column :temperatures, :parsed_at, :time
    rename_column :predictions, :parsed_at, :time
  end
end
