class AddIndexToPredcition < ActiveRecord::Migration[6.0]
  def change
    add_index :predictions, :prediction_time
    add_index :predictions, :parsed_at
  end
end
