class ChangeIndexFromLocation < ActiveRecord::Migration[6.0]
  def change
    remove_index :locations, :location_id
    add_index :locations, :location_id
    remove_index :locations, :name
    remove_index :locations, :source_id
    add_index :locations, [:source_id, :name], unique: true
  end
end
