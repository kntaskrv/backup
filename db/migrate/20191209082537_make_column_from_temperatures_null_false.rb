class MakeColumnFromTemperaturesNullFalse < ActiveRecord::Migration[6.0]
  def change
    change_column :temperatures, :temperature, :float, null: false
  end
end
