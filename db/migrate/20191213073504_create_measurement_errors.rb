class CreateMeasurementErrors < ActiveRecord::Migration[6.0]
  def up
    create_table :measurement_errors do |t|
      t.bigint :location_id, null: false
      t.bigint :measurable_id, null: false
      t.bigint :prediction_id, null: false
      t.float :weight
      t.float :value

      t.timestamps
    end
  end

  def down
    drop_table :measurement_errors
  end
end
