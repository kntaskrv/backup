class AddStateSunriseSunsetFeltLikeToPrediction < ActiveRecord::Migration[6.0]
  def change
    add_column :predictions, :state, :string
    add_column :predictions, :sunrise, :string
    add_column :predictions, :sunset, :string
    add_column :predictions, :felt_like, :float
  end
end
