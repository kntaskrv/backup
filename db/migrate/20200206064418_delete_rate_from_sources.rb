class DeleteRateFromSources < ActiveRecord::Migration[6.0]
  def up
    remove_column :sources, :rate
  end

  def down
    add_column :sources, :rate, :float
  end
end
