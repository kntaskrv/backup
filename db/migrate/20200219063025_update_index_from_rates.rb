class UpdateIndexFromRates < ActiveRecord::Migration[6.0]
  def change
    remove_index :global_rates, name: "index_global_rates_on_source_id_and_days"
    add_index :global_rates, [:source_id, :days, :location_id], unique: true
  end
end
