class AddWindAttributesToPredictions < ActiveRecord::Migration[6.0]
  def change
    add_column :predictions, :wind_speed, :float
    add_column :predictions, :wind_direction, :string
  end
end
