class UpdateIndexFromDeviations < ActiveRecord::Migration[6.0]
  def change
    remove_index :average_deviation, name: "index_average_deviations_on_source_id_and_days"
    add_index :average_deviations, [:source_id, :days, :location_id], unique: true
  end
end
