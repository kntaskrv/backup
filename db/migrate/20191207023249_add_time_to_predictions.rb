class AddTimeToPredictions < ActiveRecord::Migration[6.0]
  def change
    add_column :predictions, :time, :datetime, null: false
  end
end
