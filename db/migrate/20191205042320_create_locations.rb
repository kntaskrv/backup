class CreateLocations < ActiveRecord::Migration[6.0]
  def change
    create_table :locations do |t|
      t.string :name, null: false
      t.bigint :source_id, null: false
      t.bigint :location_id, null: true
      t.float :latitude
      t.float :longitude
      t.index :name, unique: true
      t.index :location_id, unique: true
      t.index :source_id, unique: true
    end
    add_foreign_key :locations, :sources
  end
end
