class AddSourceIdToTemperature < ActiveRecord::Migration[6.0]
  def up
    add_column :temperatures, :source_id, :bigint
  end

  def down
    remove_column :temperatures, :source_id, :bigint
  end
end
