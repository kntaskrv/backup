class AddHumidityPressureToTemperatures < ActiveRecord::Migration[6.0]
  def change
    add_column :temperatures, :humidity, :float
    add_column :temperatures, :pressure, :float
  end
end
