class CreateFactualWeathers < ActiveRecord::Migration[6.0]
  def change
    create_table :factual_weathers do |t|
      t.bigint :location_id, null: false
      t.float :temperature, null: false
      t.datetime :parsed_at, null: false
      t.float :wind_speed
      t.string :wind_direction
      t.float :humidity
      t.float :pressure

      t.timestamps

      t.index :location_id
    end
    add_foreign_key :factual_weathers, :locations
  end
end
