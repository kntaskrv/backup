class CreateAverageDeviations < ActiveRecord::Migration[6.0]
  def change
    create_table :average_deviations do |t|
      t.bigint :source_id, null: false
      t.integer :days, null: false
      t.integer :average, null: false, default: 0
      t.integer :count, null: false, default: 0
      t.float :average_deviation

      t.timestamps

      t.index [:source_id, :days], unique: true
    end
  end
end
