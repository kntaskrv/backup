class AddStateSunriseSunsetFeltLikeToTemperature < ActiveRecord::Migration[6.0]
  def change
    add_column :temperatures, :state, :string
    add_column :temperatures, :sunrise, :string
    add_column :temperatures, :sunset, :string
    add_column :temperatures, :felt_like, :float
  end
end
