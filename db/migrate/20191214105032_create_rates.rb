class CreateRates < ActiveRecord::Migration[6.0]
  def change
    create_table :rates do |t|
      t.bigint :source_id
      t.bigint :location_id
      t.float :temperature
      t.integer :days

      t.timestamps
    end
  end
end
