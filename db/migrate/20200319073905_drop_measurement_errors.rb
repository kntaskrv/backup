class DropMeasurementErrors < ActiveRecord::Migration[6.0]
  def change
    drop_table :measurement_errors
  end
end
