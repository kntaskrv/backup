class DropColumnSourceIdFromLocations < ActiveRecord::Migration[6.0]
  def up
    remove_column :locations, :source_id, :bigint
  end

  def down
    add_column :locations, :source_id, :bigint
  end
end
