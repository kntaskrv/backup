class MakeColumnFromPredictionsNullFalse < ActiveRecord::Migration[6.0]
  def change
    change_column :predictions, :temperature, :float, null: false
    change_column :predictions, :prediction_time, :datetime, null: false
  end
end
