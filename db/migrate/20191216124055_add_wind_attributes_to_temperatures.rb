class AddWindAttributesToTemperatures < ActiveRecord::Migration[6.0]
  def change
    add_column :temperatures, :wind_speed, :float
    add_column :temperatures, :wind_direction, :string
  end
end
