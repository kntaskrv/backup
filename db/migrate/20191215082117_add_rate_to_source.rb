class AddRateToSource < ActiveRecord::Migration[6.0]
  def change
    add_column :sources, :rate, :float, default: 0.0
  end
end
