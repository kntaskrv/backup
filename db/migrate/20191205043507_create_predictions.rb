class CreatePredictions < ActiveRecord::Migration[6.0]
  def change
    create_table :predictions do |t|
      t.bigint :location_id, null: false
      t.float :temperature
      t.datetime :prediction_time
      t.integer :days, null: false
      t.float :difference
      t.timestamps
      t.index :location_id
    end
    add_foreign_key :predictions, :locations
  end
end
