class AddSourceIdToPredictions < ActiveRecord::Migration[6.0]
  def change
    add_column :predictions, :source_id, :bigint
  end
end
