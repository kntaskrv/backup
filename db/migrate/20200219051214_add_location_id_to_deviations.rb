class AddLocationIdToDeviations < ActiveRecord::Migration[6.0]
  def change
    add_column :average_deviations, :location_id, :integer
  end
end
