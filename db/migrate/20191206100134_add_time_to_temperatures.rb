class AddTimeToTemperatures < ActiveRecord::Migration[6.0]
  def change
    add_column :temperatures, :time, :datetime, null: false
  end
end
