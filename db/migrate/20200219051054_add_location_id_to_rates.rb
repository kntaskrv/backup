class AddLocationIdToRates < ActiveRecord::Migration[6.0]
  def change
    add_column :global_rates, :location_id, :integer
  end
end
