class AddHumidityPreciptionPressureToPredictions < ActiveRecord::Migration[6.0]
  def change
    add_column :predictions, :humidity, :float
    add_column :predictions, :pressure, :float
    add_column :predictions, :precipitation, :float
  end
end
