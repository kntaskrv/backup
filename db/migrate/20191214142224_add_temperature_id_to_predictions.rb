class AddTemperatureIdToPredictions < ActiveRecord::Migration[6.0]
  def change
    add_column :predictions, :temperature_id, :bigint
    rename_column :predictions, :temperature, :predicted_temperature
  end
end
