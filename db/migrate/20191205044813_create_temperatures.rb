class CreateTemperatures < ActiveRecord::Migration[6.0]
  def change
    create_table :temperatures do |t|
      t.bigint :location_id, null: false
      t.float :temperature

      t.timestamps

      t.index :location_id
    end
    add_foreign_key :temperatures, :locations
  end
end
