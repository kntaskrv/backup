# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_06_092136) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "average_deviations", force: :cascade do |t|
    t.bigint "source_id", null: false
    t.integer "days", null: false
    t.integer "average", default: 0, null: false
    t.integer "count", default: 0, null: false
    t.float "average_deviation"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["source_id", "days"], name: "index_average_deviations_on_source_id_and_days", unique: true
  end

  create_table "factual_weathers", force: :cascade do |t|
    t.bigint "location_id", null: false
    t.float "temperature", null: false
    t.datetime "parsed_at", null: false
    t.float "wind_speed"
    t.string "wind_direction"
    t.float "humidity"
    t.float "pressure"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["location_id"], name: "index_factual_weathers_on_location_id"
  end

  create_table "global_rates", force: :cascade do |t|
    t.bigint "source_id", null: false
    t.integer "days", null: false
    t.integer "true_strike", default: 0, null: false
    t.integer "strike_count", default: 0, null: false
    t.float "rate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["source_id", "days"], name: "index_global_rates_on_source_id_and_days", unique: true
  end

  create_table "locations", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "location_id"
    t.float "latitude"
    t.float "longitude"
    t.index ["location_id"], name: "index_locations_on_location_id"
    t.index ["name"], name: "index_locations_on_name", unique: true
  end

  create_table "measurement_errors", force: :cascade do |t|
    t.bigint "location_id", null: false
    t.bigint "measurable_id", null: false
    t.bigint "prediction_id", null: false
    t.float "weight"
    t.float "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "predictions", force: :cascade do |t|
    t.bigint "location_id", null: false
    t.float "predicted_temperature", null: false
    t.datetime "prediction_time", null: false
    t.integer "days", null: false
    t.float "difference"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "parsed_at", null: false
    t.bigint "temperature_id"
    t.bigint "source_id"
    t.float "wind_speed"
    t.string "wind_direction"
    t.float "humidity"
    t.float "pressure"
    t.float "precipitation"
    t.index ["location_id"], name: "index_predictions_on_location_id"
    t.index ["parsed_at"], name: "index_predictions_on_parsed_at"
    t.index ["prediction_time"], name: "index_predictions_on_prediction_time"
  end

  create_table "rates", force: :cascade do |t|
    t.bigint "source_id"
    t.bigint "location_id"
    t.float "temperature"
    t.integer "days"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "sources", force: :cascade do |t|
    t.string "name", null: false
    t.string "url"
    t.index ["name"], name: "index_sources_on_name", unique: true
  end

  create_table "temperatures", force: :cascade do |t|
    t.bigint "location_id", null: false
    t.float "temperature", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "parsed_at", null: false
    t.bigint "source_id"
    t.float "wind_speed"
    t.string "wind_direction"
    t.float "humidity"
    t.float "pressure"
    t.index ["location_id"], name: "index_temperatures_on_location_id"
  end

  add_foreign_key "factual_weathers", "locations"
  add_foreign_key "predictions", "locations"
  add_foreign_key "temperatures", "locations"
end
