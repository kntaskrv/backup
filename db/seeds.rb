# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ActiveRecord::Base.transaction do

  #============================================================================================
  # Create sources and locations
  #============================================================================================

  sources_names = %w[gismeteo yandex bbc foreca openweathermap]

  Location.create(name: 'Moscow')

  sources_names.each do |name|
    Source.create(name: name)
  end

  #============================================================================================

  #============================================================================================
  # Create temperatures
  #============================================================================================

  location = Location.take
  beginning_of_today = Time.current.beginning_of_day
  today_past_hours_count = ((Time.current - beginning_of_today) / 3600).to_i + 1

  Source.all.each do |source|
    today_past_hours_count.times do |hour|
      location.temperatures.create(temperature: -rand(1.0..15.0).round(2), source: source, parsed_at: beginning_of_today + hour.hours )
    end
  end

  #============================================================================================
  # Create predictions
  #============================================================================================

  days_for_predictions_count = 5

  predictable_times = [0, 3, 6, 9, 12, 15, 18, 21]

  predicted_temperatures = location.temperatures.select { |temp| predictable_times.include?(temp.parsed_at.strftime('%H').to_i) }

  (1..5).each do |day_num|
    (0..23).each do |hour|
      predictable_times.each do |hours_count|
        Source.all.each do |source|
          location.predictions.create(
            days: day_num,
            predicted_temperature: -rand(1.0..15.0).round(2),
            prediction_time: beginning_of_today + hours_count.hours,
            parsed_at: (Date.today - day_num.days).beginning_of_day + hour.hours,
            source: source
          )
        end
      end
    end
  end


  i = 0

  (1..5).each do |day_num|
    Temperature.all.each do |temperature|
      predictable_times.each do |hour|
        location.predictions.create(
          days: day_num,
          predicted_temperature: -rand(1.0..15.0).round(2),
          prediction_time: (Date.today + day_num.days).beginning_of_day + hour.hours,
          parsed_at: temperature.parsed_at,
          source: temperature.source
        )
      end
    end
  end


  #============================================================================================
  # Create measurement errors
  #============================================================================================
  Temperature.all.each { |t| Rates::MeasurementErrors::Create.call(weather_measure_record: t) }
  #===========================================================================================
end
